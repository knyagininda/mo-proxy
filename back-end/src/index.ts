import * as $multiparty from "multiparty"
import $fs, { Stats } from "fs"
import $fsp from "fs/promises"
import $httpProxy from "http-proxy"
import * as $http from "node:http"
import $pg from "pg"
import { v4 } from "uuid"
import * as $cookie from "cookie"
import path from "path"

const S_HTTP_PORT = process.env[`HTTP_PORT`] || "8008"
const HTTP_PORT = parseInt(S_HTTP_PORT)
const PROXY_TO = process.env[`PROXY_TO`] || "http://10.110.127.76:8080"
const PG_HOST = process.env[`PG_HOST`] || "10.110.127.77"
const PG_DATABASE = process.env[`PG_DATABASE`] || "bi"
const PG_USER = process.env[`PG_USER`] || "postgres"
const PG_PASSWORD = process.env[`PG_PASSWORD`] || "536v3v4"
const PG_PORT = process.env[`PG_PORT`] || "5432"

let pool: $pg.Pool
const sessions = new Map<string, number>()

const login_html = $fs.readFileSync("./html/login.html")

var proxy = $httpProxy.createProxyServer({})
var server = $http.createServer(async (request, response) => {    
    if (request.url === "/login") {
        switch (request.method) {
            case "GET":
                response
                    .writeHead(200, { "Cache-Control": "no-cache,no-store,max-age=0,must-revalidate", "Content-Type": "text/html; charset=UTF-8" })
                    .end(login_html)
                return
            case "POST":
                const form = new $multiparty.Form()
                form.parse(request, async (err, fields: any) => {
                    if (err) {
                        response
                            .writeHead(200, {
                                "Cache-Control": "no-cache,no-store,max-age=0,must-revalidate",
                                "Content-Type": "text/html; charset=UTF-8",
                                "set-cookie": `sid_mo=0`
                            })
                            .end(login_html)
                        return
                    }
                    let auth: boolean = false
                    let client: $pg.PoolClient | null = null
                    let redirect: string = "/"
                    try {
                        client = await pool.connect()
                    } catch (e) {
                        console.log(e)
                    }
                    if (client) {
                        let qr: $pg.QueryResult | null = null
                        try {
                            qr = await client.query(`SELECT redirect FROM bi_proxy.users WHERE login=$1 AND password=$2 LIMIT 1`, [
                                fields.login[0],
                                fields.password[0]
                            ])
                        } catch (e) {
                            console.log(e)
                        }
                        client.release()
                        if (qr && qr.rowCount && qr.rowCount > 0) {
                            auth = true
                            redirect = qr.rows[0].redirect
                        }
                    }
                    if (auth) {
                        const sid_mo = v4()
                        const sid_date = Date.now() + 12 * 60 * 60 * 1000
                        sessions.set(sid_mo, sid_date)
                        response
                            .writeHead(302, {
                                "set-cookie": `sid_mo=${sid_mo}`,
                                "location": redirect
                            })
                            .end()
                        return
                    } else {
                        response
                            .writeHead(200, {
                                "Cache-Control": "no-cache,no-store,max-age=0,must-revalidate",
                                "Content-Type": "text/html; charset=UTF-8",
                                "set-cookie": `sid_mo=0`
                            })
                            .end(login_html)
                        return
                    }
                })
                return
        }
    } else if (/^\/[0-9]+\.[0-9]+\.[0-9]+\/.*$/.test(request.url!)) {
        if (request.method === "GET") {            
            const p = path.join(".", 'js', request.url!)            
            let stats: $fs.Stats | undefined = undefined
            try {
                stats = await $fsp.stat(p)
            } catch {
                console.log(request.url)
                response
                    .writeHead(404).end()
                return
            }
            if (stats && !stats.isFile()) {
                console.log(request.url)
                response
                    .writeHead(404).end()
                return
            }
            var readStream = $fs.createReadStream(p);                        
            response.writeHead(200, { "Cache-Control": "no-cache,no-store,max-age=0,must-revalidate", "Content-Type": "application/javascript" })
            readStream.pipe(response);
            return
        }
    } else {                
        const x = $cookie.parse(request.headers.cookie ? request.headers.cookie : "")
            if (x.sid_mo && x.sid_mo !== "0") {
            const sid_date = sessions.get(x.sid_mo)
            if (sid_date) {
                if (sid_date < Date.now()) {
                    sessions.delete(x.sid_mo)
                    if (request.method === "GET")
                        response
                            .writeHead(302, {
                                "location": "/login"
                            })
                            .end()
                    else response.writeHead(404).end()
                    return
                }
            } else {
                if (request.method === "GET")
                    response
                        .writeHead(302, {
                            "location": "/login"
                        })
                        .end()
                else response.writeHead(404).end()
                return
            }
        } else {
            response
            .writeHead(302, {
                "location": "/login"
            })
            .end()
            return
        }
    }
    proxy.web(request, response, { target: PROXY_TO })
})

server.listen(HTTP_PORT, () => {    
    pool = new $pg.Pool({ connectionString: `postgres://${PG_USER}:${PG_PASSWORD}@${PG_HOST}:${PG_PORT}/${PG_DATABASE}` })
})
